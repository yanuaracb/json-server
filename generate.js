/*
 * Copyright (c) 2021. AccelByte Inc. All Rights Reserved
 * This is licensed software from AccelByte Inc, for limitations
 * and restrictions contact your company contract manager.
 */

module.exports = function (){
  const faker = require("faker");
  const _ = require("lodash");
  return {
    people: _.times(10, function (n) {
      return {
        id: n,
        name: faker.name.findName()
      }
    })
  }
}
